/*
* The contents of this file are subject to the Common Public
* Attribution License Version 1.0 (the "License"); you may not use
* this file except in compliance with the License. You can read
* a copy of the License at https://opensource.org/licenses/CPAL-1.0.
*
* Software distributed under the License is distributed on an
* AS-IS basis, WITHOUT WARRANTY OF ANY KIND, either express or
* implied. See the License for the specific language governing
* rights and limitations under the License.
*
* The Original Code is part of the CYSS::Engine library.
* The Original Developer is Daniel Wanner.
*
* Copyright (c) 2015 Daniel Wanner. All Rights Reserved.
*
* Attribution Copyright Notice: Copyright (c) 2015 Daniel Wanner.
* Attribution Phrase: Achieved with CYSS::Engine.
*/
#include"..../../include/cyss/base/Matrix4.hpp"

using namespace cyss;
using namespace base;

Matrix4::Matrix4()
{
    this->m_Data = new float*[4];
    for(int x = 0; x < 4; x++)
    {
        this->m_Data[x] = new float[4];
    }
}

Matrix4::~Matrix4()
{
    for(int x = 0; x < 4; x++)
    {
        delete[] this->m_data[x];
    }
    delete[] this->m_Data;
}



